package fr.afcepf.ai107.appnomds.controller.prestation;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.EJBs;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import org.primefaces.event.SelectEvent;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;

import fr.afcepf.ai107.appnomads.entity.FeedbackMasseurEntity;
import fr.afcepf.ai107.appnomads.entity.FeedbackPartenaireEntity;
import fr.afcepf.ai107.appnomads.entity.InscriptionEntity;
import fr.afcepf.ai107.appnomads.entity.MasseurEntity;
import fr.afcepf.ai107.appnomads.entity.PrestationEntity;
import fr.afcepf.ai107.appnomads.entity.UtilisateurEntity;
import fr.afcepf.ai107.appnomds.ibusiness.prestation.PrestationIBusiness;

@ManagedBean (name = "mbPrestation")
@SessionScoped
public class PrestationManagedBean implements Serializable
{

	private static final long serialVersionUID = 1L;
	
	private PrestationEntity selectedPrestation;
	
	private Integer nbPreInscrit; 	// TODO: A Alimenter par le Business
	private Integer nbInscrit;		// TODO: A Alimenter par le Business
	
	private List<PrestationEntity> prestations;
	private List<FeedbackPartenaireEntity> feedbackPartenairesOfPresta;
	private List<FeedbackMasseurEntity> feedbackMasseursOfPresta;
	private List<InscriptionEntity> allInscriptionOnPresta;
	private Integer nbTotalInscriptions;

	@ManagedProperty (value = "#{mbAccount.utilisateur}" )
	private UtilisateurEntity utilisateur;

	

	private int idPrestation;
	

	@EJB
	private PrestationIBusiness proxyPrestationBusiness;
	
	public String showPrestation(int id) {
		
		selectedPrestation = proxyPrestationBusiness.findPrestation(id);
		
		String retour = null;
		if(selectedPrestation != null) {
			feedbackMasseursOfPresta = proxyPrestationBusiness.displayFeedbackMasseurByPrestation(selectedPrestation);
			feedbackPartenairesOfPresta = proxyPrestationBusiness.displayFeedbackPartenaireByPrestation(selectedPrestation);
			allInscriptionOnPresta = proxyPrestationBusiness.displayInscriptionsAllOnPrestation(selectedPrestation);
			setNbTotalInscriptions(proxyPrestationBusiness.getInscriptionValidOnPrestation(selectedPrestation));
			retour = "/prestation/prestationPage.xhtml?faces-redirect=true";
			
		}else {
			retour = "/prestation/ListePrestation.xhtml?faces-redirect=true";
		}
		
		return retour;
	}
	public String demanderInscritption() {
		String retour = "/prestation/ListePrestation.xhtml?faces-redirect=true";
		int etat = inscrireMasseur();
		switch (etat) {
		case -2 : retour = "/masseur/AchatMads.xhtml?faces-redirect=true";
				break;
		case -1 :retour = "/prestation/ListePrestation.xhtml?faces-redirect=true";
				break;
		case 0 :retour = "/prestation/ListePrestation.xhtml?faces-redirect=true";
				break;
		case 1 :retour = "/masseur/DashbordMasseur.xhtml?faces-redirect=true";
				break;
		}
		return retour;
	}
	public int inscrireMasseur() {
		return proxyPrestationBusiness.inscrireMasseur((MasseurEntity) utilisateur, allInscriptionOnPresta, selectedPrestation);
	}
	
	public String supprimerPrestation() {
		//TODO : implementer la methode supprimer
		
		proxyPrestationBusiness.deletePrestation(selectedPrestation);
		prestations = proxyPrestationBusiness.getPrestations(true, true, true);
		String retour = "/prestation/ListePrestation.xhtml?faces-redirect=true";
		
		return retour;
	}
	public String modifierPrestation() {
		//TODO : implementer mettre a jour une presta
		proxyPrestationBusiness.update(selectedPrestation);
		prestations = proxyPrestationBusiness.getPrestations(true, true, true);
		String retour = "/prestation/ListePrestation.xhtml?faces-redirect=true";
		
		return retour;
	}
	
	public void onDateSelect(SelectEvent event) {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        facesContext.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Date Selected", format.format(event.getObject())));
    }
	
	
	// AFFICHER LISTE DES PRESTATIONS   /////
	public String showListPrestation() {
		
		return "/AppNomadsWeb/prestation/ListePrestation.xhtml?faces-redirect=true";
	}
	
	public void btnshowList(Boolean past,Boolean today, Boolean futur){
		prestations = null;
		prestations = proxyPrestationBusiness.getPrestations(past, today, futur);
				
	}
	
	public int inscriptionByPrestation(PrestationEntity prestation) {
		return proxyPrestationBusiness.masseurInscrit(prestation);
	}
	public int preInscriptionByPrestation(PrestationEntity prestation) {
		return proxyPrestationBusiness.masseurPreInscrit(prestation);
	}
	
	
	
	@PostConstruct
	public void init() {
		
		prestations = proxyPrestationBusiness.getPrestations(true, true, true);
	
	}
	
	

	//////////////////// Getter Setter ///////////////////////////////////////
	

	public PrestationEntity getSelectedPrestation() {
		return selectedPrestation;
	}

	public void setSelectedPrestation(PrestationEntity selectedPrestation) {
		this.selectedPrestation = selectedPrestation;
	}

	

	public List<PrestationEntity> getPrestations() {
		return prestations;
	}

	public void setPrestations(List<PrestationEntity> prestations) {
		this.prestations = prestations;
	}


	public int getIdPrestation() {
		return idPrestation;
	}

	public void setIdPrestation(int idPrestation) {
		this.idPrestation = idPrestation;
	}



	
	public List<FeedbackPartenaireEntity> getFeedbackPartenairesOfPresta() {
		return feedbackPartenairesOfPresta;
	}

	public void setFeedbackPartenairesOfPresta(List<FeedbackPartenaireEntity> feedbackPartenairesOfPresta) {
		this.feedbackPartenairesOfPresta = feedbackPartenairesOfPresta;
	}

	public List<FeedbackMasseurEntity> getFeedbackMasseursOfPresta() {
		return feedbackMasseursOfPresta;
	}

	public void setFeedbackMasseursOfPresta(List<FeedbackMasseurEntity> feedbackMasseursOfPresta) {
		this.feedbackMasseursOfPresta = feedbackMasseursOfPresta;
	}

	public List<InscriptionEntity> getAllInscriptionOnPresta() {
		return allInscriptionOnPresta;
	}

	public void setAllInscriptionOnPresta(List<InscriptionEntity> allInscriptionOnPresta) {
		this.allInscriptionOnPresta = allInscriptionOnPresta;
	}



	public Integer getNbTotalInscriptions() {
		return nbTotalInscriptions;
	}

	public void setNbTotalInscriptions(Integer nbTotalInscriptions) {
		this.nbTotalInscriptions = nbTotalInscriptions;
	}



	public Integer getNbPreInscrit() {
		return nbPreInscrit;
	}

	public void setNbPreInscrit(Integer nbPreInscrit) {
		this.nbPreInscrit = nbPreInscrit;
	}

	public Integer getNbInscrit() {
		return nbInscrit;
	}

	public void setNbInscrit(Integer nbInscrit) {
		this.nbInscrit = nbInscrit;
	}
	public UtilisateurEntity getUtilisateur() {
		return utilisateur;
	}

	public void setUtilisateur(UtilisateurEntity utilisateur) {
		this.utilisateur = utilisateur;
	}
	
	
	
	//****  ***//
	
	
	/////////////////////////////////////////////////////////////////////////

}
