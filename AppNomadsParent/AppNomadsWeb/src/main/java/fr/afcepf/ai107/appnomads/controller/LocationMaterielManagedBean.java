package fr.afcepf.ai107.appnomads.controller;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import fr.afcepf.ai107.appnomads.IBusiness.LocationMaterielIBusiness;
import fr.afcepf.ai107.appnomads.IBusiness.MasseurIBusiness;
import fr.afcepf.ai107.appnomads.entity.EmpruntMaterielEntity;
import fr.afcepf.ai107.appnomads.entity.LibelleMaterielEntity;
import fr.afcepf.ai107.appnomads.entity.MasseurEntity;
import fr.afcepf.ai107.appnomads.entity.PrestationEntity;
import fr.afcepf.ai107.appnomads.entity.UtilisateurEntity;

@ManagedBean (name="mbLocationMateriel")
@ViewScoped
public class LocationMaterielManagedBean implements Serializable{

	
	private static final long serialVersionUID = 1L;
	
	private MasseurEntity masseur = new MasseurEntity();
	private PrestationEntity prestation = new PrestationEntity();

	private List<LibelleMaterielEntity> listeMateriel;
	private EmpruntMaterielEntity empruntMateriel = new EmpruntMaterielEntity();
	private Date dateDebutEmpruntMateriel;
	private Date dateFinEmpruntMateriel;
	private Integer quantiteMateriel;
	private String dateDebutEmprunt;
	private String dateFinEmprunt;

	private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
	
	private LibelleMaterielEntity materiel = new LibelleMaterielEntity();
	
	@ManagedProperty (value = "#{mbAccount.utilisateur}")
	private UtilisateurEntity user;
	
	@EJB 
	private LocationMaterielIBusiness proxyLocationMaterielBu;

	@EJB
	private MasseurIBusiness proxyMasseurBu;
	
	public String AjouterMateriel() {
		SimpleDateFormat formatterDate = new SimpleDateFormat("yyyy-MM-dd");
		try {
			empruntMateriel.setDateDebutEmprunt(formatterDate.parse(dateDebutEmprunt));
			empruntMateriel.setDateFinEmpruntPrevue(formatterDate.parse(dateFinEmprunt));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		System.out.println("ajouter materiel");
	empruntMateriel.setMasseurEntity((MasseurEntity) user);
	empruntMateriel.setLibelleEntity(materiel);
	empruntMateriel = proxyLocationMaterielBu.addEmpruntMateriel (empruntMateriel);
	empruntMateriel = new EmpruntMaterielEntity();
	return "/masseur/DashbordMasseur.xhtml?faces-redirect=true";
	}
		
	@PostConstruct
	public void init() {
	listeMateriel = proxyLocationMaterielBu.displayAllMateriel();
	}

	public LocationMaterielManagedBean() {
	}
	
//-----------------//Getters Setters//-----------------------------------------------
	public EmpruntMaterielEntity getEmpruntMateriel() {
		return empruntMateriel;
	}

	public void setEmpruntMateriel(EmpruntMaterielEntity empruntMateriel) {
		this.empruntMateriel = empruntMateriel;
	}

	public List<LibelleMaterielEntity> getListeMateriel() {
		return listeMateriel;
	}

	public void setListeMateriel(List<LibelleMaterielEntity> listeMateriel) {
		this.listeMateriel = listeMateriel;
	}

	public LocationMaterielIBusiness getProxyLocationMaterielBu() {
		return proxyLocationMaterielBu;
	}

	public void setProxyLocationMaterielBu(LocationMaterielIBusiness proxyLocationMaterielBu) {
		this.proxyLocationMaterielBu = proxyLocationMaterielBu;
	}
	

	public Integer getQuantiteMateriel() {
		return quantiteMateriel;
	}

	public void setQuantiteMateriel(Integer quantiteMateriel) {
		this.quantiteMateriel = quantiteMateriel;
	}

	public MasseurEntity getMasseur() {
		return masseur;
	}

	public void setMasseur(MasseurEntity masseur) {
		this.masseur = masseur;
	}

	public UtilisateurEntity getUser() {
		return user;
	}

	public void setUser(UtilisateurEntity user) {
		this.user = user;
	}

	public MasseurIBusiness getProxyMasseurBu() {
		return proxyMasseurBu;
	}

	public void setProxyMasseurBu(MasseurIBusiness proxyMasseurBu) {
		this.proxyMasseurBu = proxyMasseurBu;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Date getDateDebutEmpruntMateriel() {
		return dateDebutEmpruntMateriel;
	}

	public void setDateDebutEmpruntMateriel(Date dateDebutEmpruntMateriel) {
		this.dateDebutEmpruntMateriel = dateDebutEmpruntMateriel;
	}

	public Date getDateFinEmpruntMateriel() {
		return dateFinEmpruntMateriel;
	}

	public void setDateFinEmpruntMateriel(Date dateFinEmpruntMateriel) {
		this.dateFinEmpruntMateriel = dateFinEmpruntMateriel;
	}

	public PrestationEntity getPrestation() {
		return prestation;
	}

	public void setPrestation(PrestationEntity prestation) {
		this.prestation = prestation;
	}

	public LibelleMaterielEntity getMateriel() {
		return materiel;
	}

	public void setMateriel(LibelleMaterielEntity materiel) {
		this.materiel = materiel;
	}

	public String getDateDebutEmprunt() {
		return dateDebutEmprunt;
	}

	public void setDateDebutEmprunt(String dateDebutEmprunt) {
		this.dateDebutEmprunt = dateDebutEmprunt;
	}

	public String getDateFinEmprunt() {
		return dateFinEmprunt;
	}

	public void setDateFinEmprunt(String dateFinEmprunt) {
		this.dateFinEmprunt = dateFinEmprunt;
	}
	
	
	}
