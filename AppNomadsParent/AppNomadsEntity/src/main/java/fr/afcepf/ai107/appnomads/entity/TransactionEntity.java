package fr.afcepf.ai107.appnomads.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "transaction")
public class TransactionEntity implements Serializable {
	
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue (strategy = GenerationType.IDENTITY)
	private Integer id;
	@Column(name = "quantite_pack")
	private int quantitePack;
	@Column(name = "date_achat")
	private Date dateAchat;
	
	@ManyToOne
	@JoinColumn (referencedColumnName = "id")
	private TypePacksEntity typePacksEntity;
	
	@ManyToOne
	@JoinColumn (referencedColumnName = "id")
	private MasseurEntity masseur;
	
	public TransactionEntity(Integer id, int quantitePack, Date dateAchat, TypePacksEntity typePacksEntity,
			MasseurEntity masseur) {
		super();
		this.id = id;
		this.quantitePack = quantitePack;
		this.dateAchat = dateAchat;
		this.typePacksEntity = typePacksEntity;
		this.masseur = masseur;
	}
	
	public TransactionEntity() {
		super();
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public int getQuantitePack() {
		return quantitePack;
	}
	public void setQuantitePack(int quantitePack) {
		this.quantitePack = quantitePack;
	}
	public Date getDateAchat() {
		return dateAchat;
	}
	public void setDateAchat(Date dateAchat) {
		this.dateAchat = dateAchat;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public TypePacksEntity getTypePacksEntity() {
		return typePacksEntity;
	}
	public void setTypePacksEntity(TypePacksEntity typePacksEntity) {
		this.typePacksEntity = typePacksEntity;
	}
	public MasseurEntity getMasseur() {
		return masseur;
	}
	public void setMasseur(MasseurEntity masseur) {
		this.masseur = masseur;
	}

}
