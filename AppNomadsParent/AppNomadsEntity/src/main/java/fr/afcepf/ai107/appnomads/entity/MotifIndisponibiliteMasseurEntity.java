package fr.afcepf.ai107.appnomads.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name= "motif_indisponibilite_masseur")
public class MotifIndisponibiliteMasseurEntity implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue (strategy = GenerationType.IDENTITY)
	private Integer id;
	@Column(name = "motif")
	private String motif;
	
	@OneToMany (mappedBy = "motif", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private List<IndisponibiliteMasseurEntity> indisponibiliteMasseurEntity;
	
	public MotifIndisponibiliteMasseurEntity(Integer id, String motif) {
		super();
		this.id = id;
		this.motif = motif;
	}
	public MotifIndisponibiliteMasseurEntity(Integer id, String motif,
			List<IndisponibiliteMasseurEntity> indisponibiliteMasseurEntity) {
		super();
		this.id = id;
		this.motif = motif;
		this.indisponibiliteMasseurEntity = indisponibiliteMasseurEntity;
	}
	public MotifIndisponibiliteMasseurEntity() {
		super();
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getMotif() {
		return motif;
	}
	public void setMotif(String motif) {
		this.motif = motif;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public List<IndisponibiliteMasseurEntity> getIndisponibiliteMasseurEntity() {
		return indisponibiliteMasseurEntity;
	}
	public void setIndisponibiliteMasseurEntity(List<IndisponibiliteMasseurEntity> indisponibiliteMasseurEntity) {
		this.indisponibiliteMasseurEntity = indisponibiliteMasseurEntity;
	}

}
