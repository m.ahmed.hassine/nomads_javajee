package fr.afcepf.ai107.appnomads.controller.masseur;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.ViewScoped;

import fr.afcepf.ai107.appnomads.IBusiness.MasseurIBusiness;
import fr.afcepf.ai107.appnomads.entity.MasseurEntity;
import fr.afcepf.ai107.appnomads.entity.TransactionEntity;
import fr.afcepf.ai107.appnomads.entity.TypePacksEntity;
import fr.afcepf.ai107.appnomads.entity.UtilisateurEntity;

@ManagedBean (name = "mbMads")
@ViewScoped
public class AchatMadsManagedBean implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private TransactionEntity selectedTransaction;
	private TransactionEntity transaction = new TransactionEntity();
	private List<TransactionEntity> listeTransactions;
	private TypePacksEntity selectedPack;
	private List<TypePacksEntity> listePack;
	
	@ManagedProperty (value = "#{mbAccount.utilisateur}" )
	private UtilisateurEntity utilisateur;
	private int quantite;
//	private float montant = 0;
	private int solde;
	
	@EJB
	private MasseurIBusiness proxyMasseurBu;
	
	public String creerTransaction() {
		System.out.println("Dans la methode");
		String retour= "/prestation/ListePrestation.xhtml?faces-redirect=true";
		transaction.setDateAchat(new Date());
		transaction.setMasseur((MasseurEntity) utilisateur);
		transaction.setQuantitePack(quantite);
		transaction.setTypePacksEntity(selectedPack);
		proxyMasseurBu.addTransaction(transaction);
		solde = (int) proxyMasseurBu.getSoldeByMasseur((MasseurEntity) utilisateur);
		transaction = new TransactionEntity();
		return retour;
	}
	
	@PostConstruct
	public void init() {
		listePack = proxyMasseurBu.getListeTypePacks();
		solde = (int) proxyMasseurBu.getSoldeByMasseur((MasseurEntity) utilisateur);
	}
//	public float calculerMontant () {
//		return quantite * selectedPack.getPrix();
//	}
	public TransactionEntity getSelectedTransaction() {
		return selectedTransaction;
	}
	
	public void setSelectedTransaction(TransactionEntity selectedTransaction) {
		this.selectedTransaction = selectedTransaction;
	}

	public List<TransactionEntity> getListeTransactions() {
		return listeTransactions;
	}

	public void setListeTransactions(List<TransactionEntity> listeTransactions) {
		this.listeTransactions = listeTransactions;
	}

	public TypePacksEntity getSelectedPack() {
		return selectedPack;
	}

	public void setSelectedPack(TypePacksEntity selectedPack) {
		this.selectedPack = selectedPack;
	}

	public TransactionEntity getTransaction() {
		return transaction;
	}

	public void setTransaction(TransactionEntity transaction) {
		this.transaction = transaction;
	}

	public List<TypePacksEntity> getListePack() {
		return listePack;
	}

	public void setListePack(List<TypePacksEntity> listePack) {
		this.listePack = listePack;
	}

	public int getQuantite() {
		return quantite;
	}

	public void setQuantite(int quantite) {
		this.quantite = quantite;
	}

//	public float getMontant() {
//		montant = calculerMontant();
//		System.out.println(montant);
//		return montant;
//		
//	}
//	public void setMontant(float montant) {
//		this.montant = montant;
//	}

	public UtilisateurEntity getUtilisateur() {
		return utilisateur;
	}

	public void setUtilisateur(UtilisateurEntity utilisateur) {
		this.utilisateur = utilisateur;
	}

	public int getSolde() {
		return solde;
	}

	public void setSolde(int solde) {
		this.solde = solde;
	}

}
