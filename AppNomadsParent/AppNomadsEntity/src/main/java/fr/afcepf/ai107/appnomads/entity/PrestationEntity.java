package fr.afcepf.ai107.appnomads.entity;

import java.io.Serializable;
import java.sql.Time;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table (name = "prestation")
public class PrestationEntity implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue (strategy = GenerationType.IDENTITY)
	@Column (name = "id", nullable = false)
	private Integer id;
	@Column (name = "libelle")
	private String libelle;
	@Column (name = "date_prestation")
	private Date datePrestation;
	@Column (name = "heure_debut")
	private Time heureDebut;
	@Column (name = "heure_fin")
	private Time heureFin;
	@Column (name = "duree_demandee_par_massage")
	private Date dureeDemandeeParMassage;
	@Column (name = "nb_masseur")
	private Integer nbMasseur;
	@Column (name = "nb_chaises_ergonomique")
	private Integer nbChaiseErgonomiques;
	@Column (name = "couts_inscription")
	private Float coutsInscription;
	@Column (name = "numero_facture")
	private String numeroFacture;
	@Column (name = "date_facture")
	private Date dateFacture;
	@Column (name = "montant_facture")
	private Float montantFacture;
	@Column (name = "montant_remuneration")
	private Float montantRemuneration;
	@Column (name = "date_paiement_facture")
	private Date paimentFacture;
	@Column (name = "date_annulation")
	private Date annulation;
	@Column (name = "specificite")
	private String specificite;
	@Column (name = "id_prestation_mere")
	private Integer idPrestationMere;
	

	
	@ManyToOne
	@JoinColumn (referencedColumnName = "id", nullable = true)
	private ReccurenceEntity reccurence;
	@ManyToOne
	@JoinColumn (referencedColumnName = "id", nullable = true)
	private TypePaiementEntity typePaiment;
	@ManyToOne
	@JoinColumn (referencedColumnName = "id", nullable = true)
	private MotifAnnulationEntity motifAnnulation;	
	@ManyToOne
	@JoinColumn (referencedColumnName = "id", nullable = true) // Il faudra le passer a false une fois la stack finis
	private AdresseEntity adresseEntity;
	@ManyToOne
	@JoinColumn (referencedColumnName = "id", nullable = true) // Il faudra le passer a false une fois la stack finis
	private PartenaireEntity partenaireEntity;
	
	@OneToMany (mappedBy = "prestationEntity",cascade = CascadeType.ALL, fetch = FetchType.LAZY) // EAGER Pour charger toutes les inscriptions du masseur au chargement de la prestation
	private List<InscriptionEntity> inscriptionEntity;
	
	@OneToMany (mappedBy = "prestationEntity", cascade = CascadeType.ALL, fetch = FetchType.LAZY) // EAGER Pour charger tout les feeback du masseur au chargement de la prestation
	private List<FeedbackMasseurEntity> feedbackMasseurEntity;
	
	@OneToMany (mappedBy = "prestationEntity", cascade = CascadeType.ALL, fetch = FetchType.LAZY) // EAGER Pour charger tout les feeback du partenaire au chargement de la prestation
	private List<FeedbackPartenaireEntity> feedbackPartenaireEntity;
	
	@OneToMany (mappedBy = "prestationEntity", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private List<EmpruntMaterielEntity> empruntMaterielEntity;
	
	
	

	/**
	 * @param id
	 * @param libelle
	 * @param datePrestation
	 * @param heureDebut
	 * @param heureFin
	 * @param dureeDemandeeParMassage
	 * @param nbMasseur
	 * @param nbChaiseErgonomiques
	 * @param coutsInscription
	 * @param numeroFacture
	 * @param dateFacture
	 * @param montantFacture
	 * @param montantRemuneration
	 * @param paimentFacture
	 * @param annulation
	 * @param specificite
	 * @param idPrestationMere
	 * @param reccurence
	 * @param typePaiment
	 * @param motifAnnulation
	 * @param adresseEntity
	 * @param partenaireEntity
	 * @param inscriptionEntity
	 * @param feedbackMasseurEntity
	 * @param feedbackPartenaireEntity
	 * @param empruntMaterielEntity
	 */
	public PrestationEntity(Integer id, String libelle, Date datePrestation, Time heureDebut, Time heureFin,
			Date dureeDemandeeParMassage, Integer nbMasseur, Integer nbChaiseErgonomiques, Float coutsInscription,
			String numeroFacture, Date dateFacture, Float montantFacture, Float montantRemuneration,
			Date paimentFacture, Date annulation, String specificite, Integer idPrestationMere,
			ReccurenceEntity reccurence, TypePaiementEntity typePaiment, MotifAnnulationEntity motifAnnulation,
			AdresseEntity adresseEntity, PartenaireEntity partenaireEntity, List<InscriptionEntity> inscriptionEntity,
			List<FeedbackMasseurEntity> feedbackMasseurEntity, List<FeedbackPartenaireEntity> feedbackPartenaireEntity,
			List<EmpruntMaterielEntity> empruntMaterielEntity)
	{
		super();
		this.id = id;
		this.libelle = libelle;
		this.datePrestation = datePrestation;
		this.heureDebut = heureDebut;
		this.heureFin = heureFin;
		this.dureeDemandeeParMassage = dureeDemandeeParMassage;
		this.nbMasseur = nbMasseur;
		this.nbChaiseErgonomiques = nbChaiseErgonomiques;
		this.coutsInscription = coutsInscription;
		this.numeroFacture = numeroFacture;
		this.dateFacture = dateFacture;
		this.montantFacture = montantFacture;
		this.montantRemuneration = montantRemuneration;
		this.paimentFacture = paimentFacture;
		this.annulation = annulation;
		this.specificite = specificite;
		this.idPrestationMere = idPrestationMere;
		this.reccurence = reccurence;
		this.typePaiment = typePaiment;
		this.motifAnnulation = motifAnnulation;
		this.adresseEntity = adresseEntity;
		this.partenaireEntity = partenaireEntity;
		this.inscriptionEntity = inscriptionEntity;
		this.feedbackMasseurEntity = feedbackMasseurEntity;
		this.feedbackPartenaireEntity = feedbackPartenaireEntity;
		this.empruntMaterielEntity = empruntMaterielEntity;
	}
	
	
	public PrestationEntity()
	{
		super();
	}

	
	static Integer nullEgalZero(Integer i) {
		if(i == null) {
			return 0;
		}
		return i;
	}
	static Float nullEgalZero(Float i) {
		if(i == null) {
			return (float) 0;
		}
		return i;
	}


	public int getIdPrestation() {
		return id;
	}
	public void setIdPrestation(int idPrestation) {
		this.id = idPrestation;
	}
	public String getLibelle() {
		return libelle;
	}
	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}
	public String getDatePrestation() {
		return  new SimpleDateFormat("dd/MM/yyyy").format(datePrestation);
	}
	public Date getDatePrestationDate(){
		Date date = null;
		try {
		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
	    	    date = formatter.parse(formatter.format(datePrestation));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return date;
	}
	public void setDatePrestation(Date datePrestation) {
		this.datePrestation = datePrestation;
	}
	
	public Date getHeureDebut() {
		return heureDebut;
	}
	public void setHeureDebut(Time heureDebut) {
		this.heureDebut = heureDebut;
	}
	public Date getHeureFin() {
		return heureFin;
	}
	public void setHeureFin(Time heureFin) {
		this.heureFin = heureFin;
	}
	public Date getDureeDemandeeParMassage() {
		return dureeDemandeeParMassage;
	}
	public void setDureeDemandeeParMassage(Date dureeDemandeeParMassage) {
		this.dureeDemandeeParMassage = dureeDemandeeParMassage;
	}
	public int getNbMasseur() {
		
		return nullEgalZero(nbMasseur);
	}
	public void setNbMasseur(int nbMasseur) {
		this.nbMasseur = nbMasseur;
	}
	public int getNbChaiseErgonomiques() {
		return nullEgalZero(nbChaiseErgonomiques);
	}
	public void setNbChaiseErgonomiques(Integer nbChaiseErgonomiques) {
		this.nbChaiseErgonomiques = nbChaiseErgonomiques;
	}
	public float getCoutsInscription() {
		
		return nullEgalZero(coutsInscription);
	}
	public void setCoutsInscription(float coutsInscription) {
		this.coutsInscription = coutsInscription;
	}
	public String getNumeroFacture() {
		return numeroFacture;
	}
	public void setNumeroFacture(String numeroFacture) {
		this.numeroFacture = numeroFacture;
	}
	public Date getDateFacture() {
		return dateFacture;
	}
	public void setDateFacture(Date dateFacture) {
		this.dateFacture = dateFacture;
	}
	public float getMontantFacture() {
		return nullEgalZero(montantFacture);
	}
	public void setMontantFacture(float montantFacture) {
		this.montantFacture = montantFacture;
	}
	public float getMontantRemuneration() {
		return nullEgalZero(montantRemuneration);
	}
	public void setMontantRemuneration(float montantRemuneration) {
		this.montantRemuneration = montantRemuneration;
	}
	public Date getPaimentFacture() {
		return paimentFacture;
	}
	public void setPaimentFacture(Date paimentFacture) {
		this.paimentFacture = paimentFacture;
	}
	public Date getAnnulation() {
		return annulation;
	}
	public void setAnnulation(Date annulation) {
		this.annulation = annulation;
	}
	public String getSpécificite() {
		return specificite;
	}
	public void setSpécificite(String spécificite) {
		this.specificite = spécificite;
	}
	public int getIdPrestationMere() {
		return idPrestationMere;
	}
	public void setIdPrestationMere(int idPrestationMere) {
		this.idPrestationMere = idPrestationMere;
	}
	public ReccurenceEntity getReccurence() {
		return reccurence;
	}
	public void setReccurence(ReccurenceEntity reccurence) {
		this.reccurence = reccurence;
	}
	public TypePaiementEntity getTypePaiment() {
		return typePaiment;
	}
	public void setTypePaiment(TypePaiementEntity typePaiment) {
		this.typePaiment = typePaiment;
	}
	public MotifAnnulationEntity getMotifAnnulation() {
		return motifAnnulation;
	}
	public void setMotifAnnulation(MotifAnnulationEntity motifAnnulation) {
		this.motifAnnulation = motifAnnulation;
	}
	public Integer getId() {
		return id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public String getSpecificite() {
		return specificite;
	}


	public void setSpecificite(String specificite) {
		this.specificite = specificite;
	}


	public AdresseEntity getAdresseEntity() {
		return adresseEntity;
	}


	public void setAdresseEntity(AdresseEntity adresseEntity) {
		this.adresseEntity = adresseEntity;
	}


	public PartenaireEntity getPartenaireEntity() {
		return partenaireEntity;
	}


	public void setPartenaireEntity(PartenaireEntity partenaireEntity) {
		this.partenaireEntity = partenaireEntity;
	}


	public List<InscriptionEntity> getInscriptionEntity() {
		return inscriptionEntity;
	}


	public void setInscriptionEntity(List<InscriptionEntity> inscriptionEntity) {
		this.inscriptionEntity = inscriptionEntity;
	}


	public List<FeedbackMasseurEntity> getFeedbackMasseurEntity() {
		return feedbackMasseurEntity;
	}


	public void setFeedbackMasseurEntity(List<FeedbackMasseurEntity> feedbackMasseurEntity) {
		this.feedbackMasseurEntity = feedbackMasseurEntity;
	}


	public List<FeedbackPartenaireEntity> getFeedbackPartenaireEntity() {
		return feedbackPartenaireEntity;
	}


	public void setFeedbackPartenaireEntity(List<FeedbackPartenaireEntity> feedbackPartenaireEntity) {
		this.feedbackPartenaireEntity = feedbackPartenaireEntity;
	}


	public List<EmpruntMaterielEntity> getEmpruntMaterielEntity() {
		return empruntMaterielEntity;
	}


	public void setEmpruntMaterielEntity(List<EmpruntMaterielEntity> empruntMaterielEntity) {
		this.empruntMaterielEntity = empruntMaterielEntity;
	}


	public void setNbMasseur(Integer nbMasseur) {
		this.nbMasseur = nbMasseur;
	}


	public void setCoutsInscription(Float coutsInscription) {
		this.coutsInscription = coutsInscription;
	}


	public void setMontantFacture(Float montantFacture) {
		this.montantFacture = montantFacture;
	}


	public void setMontantRemuneration(Float montantRemuneration) {
		this.montantRemuneration = montantRemuneration;
	}


	public void setIdPrestationMere(Integer idPrestationMere) {
		this.idPrestationMere = idPrestationMere;
	}


	@Override
	public String toString() {
		return "PrestationEntity [id=" + id + ", libelle=" + libelle + ", datePrestation=" + datePrestation
				+ ", heureDebut=" + heureDebut + ", heureFin=" + heureFin + ", dureeDemandeeParMassage="
				+ dureeDemandeeParMassage + ", nbMasseur=" + nbMasseur + ", nbChaiseErgonomiques="
				+ nbChaiseErgonomiques + ", coutsInscription=" + coutsInscription + ", numeroFacture=" + numeroFacture
				+ ", dateFacture=" + dateFacture + ", montantFacture=" + montantFacture + ", montantRemuneration="
				+ montantRemuneration + ", paimentFacture=" + paimentFacture + ", annulation=" + annulation
				+ ", specificite=" + specificite + ", idPrestationMere=" + idPrestationMere + "]";
	}


}
