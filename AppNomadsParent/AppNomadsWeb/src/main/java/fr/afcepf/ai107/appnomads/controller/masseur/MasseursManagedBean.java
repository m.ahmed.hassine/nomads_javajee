package fr.afcepf.ai107.appnomads.controller.masseur;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import fr.afcepf.ai107.appnomads.IBusiness.MasseurIBusiness;
import fr.afcepf.ai107.appnomads.entity.HistoriqueActiviteMasseurEntity;
import fr.afcepf.ai107.appnomads.entity.MasseurEntity;
import fr.afcepf.ai107.appnomads.entity.PreferencesEntity;
import fr.afcepf.ai107.appnomads.entity.PrestationEntity;
import fr.afcepf.ai107.appnomads.entity.UtilisateurEntity;
import fr.afcepf.ai107.appnomds.ibusiness.prestation.PrestationIBusiness;

@ManagedBean (name = "mbMasseurs")
@SessionScoped
public class MasseursManagedBean implements Serializable{

	private static final long serialVersionUID = 1L;

	private MasseurEntity selectedMasseur;
	private PrestationEntity selectedPrestation;
	
	private List<MasseurEntity> listeMasseurs;
	private List<PrestationEntity> listePrestations = new ArrayList<PrestationEntity>();
	private List<PreferencesEntity> listePreferences = new ArrayList<PreferencesEntity>();
		
	@EJB
	private MasseurIBusiness proxyMasseurBu;
	
	@EJB
	private PrestationIBusiness proxyPrestationBu;
	
	@ManagedProperty (value = "#{mbAccount.utilisateur}" )
	private UtilisateurEntity utilisateur;
	
	@ManagedProperty (value = "mbAccount.template")
	private String template;
	private String activite;
	private String niveau;
	private String statut;

	private List<HistoriqueActiviteMasseurEntity> listeHistoriqueActiviteMasseur;
	




	@PostConstruct
	public void init() {
		listeMasseurs = proxyMasseurBu.findAll();
		listePrestations = proxyMasseurBu.getListePrestationsByMasseur(connectedMasseur(utilisateur.getId()));
	}
	
	public MasseurEntity connectedMasseur(Integer id) {
		
		MasseurEntity masseurConnected = proxyMasseurBu.getMasseurById(id);
		return masseurConnected;
	}
	
	public String feedbackPage(int id) {
		String retour = null;
		selectedPrestation = proxyPrestationBu.findPrestation(id);
		if(selectedPrestation != null) {
			retour = "/masseur/FeedbackMasseur.xhtml?faces-redirect=true";
		}
		
		return retour;
	}
	
	public String updateMasseur() {
		proxyMasseurBu.updateMasseur(selectedMasseur);
		return "/masseur/ListeMasseurs.xhtml?faces-redirect=true";
	}
		
	public MasseurEntity getSelectedMasseur() {
		return selectedMasseur;
	}
	public void setSelectedMasseur(MasseurEntity selectedMasseur) {
		this.selectedMasseur = selectedMasseur;
	}
	public List<MasseurEntity> getListeMasseurs() {
		return listeMasseurs;
	}
	public void setListeMasseurs(List<MasseurEntity> listeMasseurs) {
		this.listeMasseurs = listeMasseurs;
	}
	
	
	/// Chargement info profil masseur


	public String showMasseur(int id) {
		selectedMasseur = proxyMasseurBu.getMasseurById(id);		
		String retour = null;
		if(selectedMasseur != null) {
			retour = "/masseur/ConsulterMasseur.xhtml?faces-redirect=true";
			listePrestations = proxyMasseurBu.getListePrestationsByMasseur(selectedMasseur);
			listePreferences = proxyMasseurBu.getListePreferencesByMasseur(selectedMasseur);
			listeHistoriqueActiviteMasseur = proxyMasseurBu.getListeHistoriqueActiviteByMasseur(selectedMasseur);
			activite = isActif(selectedMasseur);
			niveau = getNiveauMasseur(selectedMasseur);
			statut = getStatutMasseur(selectedMasseur);
		}else {
			retour = "/masseur/ListeMasseurs.xhtml?faces-redirect=true";
		}
		
		return retour;
	}
	
	public String isActif(MasseurEntity selectedMasseur) {
		boolean isActif = proxyMasseurBu.isActif(selectedMasseur);
		if (isActif) {
			return "Actif";
		}
		else {
			return "Inactif";
		}
	}
	public String desactiverMasseur() {
		proxyMasseurBu.desactiverMasseur(selectedMasseur);
		listeMasseurs = proxyMasseurBu.findAll();
		return "/masseur/ListeMasseurs.xhtml?faces-redirect=true";
	}
	public String getNiveauMasseur(MasseurEntity selectedMasseur) {
		return proxyMasseurBu.getNiveauByMasseur(selectedMasseur).getLibelle();
	}
	
	public String getStatutMasseur(MasseurEntity selectedMasseur) {
		return proxyMasseurBu.getStatutByMasseur(selectedMasseur).getLibelle();
	}
	public MasseurIBusiness getProxyMasseurBu() {
		return proxyMasseurBu;
	}
	public void setProxyMasseurBu(MasseurIBusiness proxyMasseurBu) {
		this.proxyMasseurBu = proxyMasseurBu;
	}
	
	public List<PrestationEntity> getListePrestations() {
		return listePrestations;
	}
	public void setListePrestations(List<PrestationEntity> listePrestations) {
		this.listePrestations = listePrestations;
	}

	public List<PreferencesEntity> getlistePreferences() {
		return listePreferences;
	}

	public void setlistePreferences(List<PreferencesEntity> listePreferences) {
		this.listePreferences = listePreferences;
	}

	public List<PreferencesEntity> getListePreferences() {
		return listePreferences;
	}

	public void setListePreferences(List<PreferencesEntity> listePreferences) {
		this.listePreferences = listePreferences;
	}
	
	public UtilisateurEntity getUtilisateur() {
		return utilisateur;
	}

	public void setUtilisateur(UtilisateurEntity utilisateur) {
		this.utilisateur = utilisateur;
	}
	
	public String getTemplate() {
		return template;
	}

	public void setTemplate(String template) {
		this.template = template;
	}




	public String getActivite() {
		return activite;
	}




	public void setActivite(String activite) {
		this.activite = activite;
	}




	public String getNiveau() {
		return niveau;
	}




	public void setNiveau(String niveau) {
		this.niveau = niveau;
	}




	public static long getSerialversionuid() {
		return serialVersionUID;
	}




	public String getStatut() {
		return statut;
	}




	public void setStatut(String statut) {
		this.statut = statut;
	}


	public List<HistoriqueActiviteMasseurEntity> getListeHistoriqueActiviteMasseur() {
		return listeHistoriqueActiviteMasseur;
	}


	public void setListeHistoriqueActiviteMasseur(List<HistoriqueActiviteMasseurEntity> listeHistoriqueActiviteMasseur) {
		this.listeHistoriqueActiviteMasseur = listeHistoriqueActiviteMasseur;
	}

	public PrestationEntity getSelectedPrestation() {
		return selectedPrestation;
	}

	public void setSelectedPrestation(PrestationEntity selectedPrestation) {
		this.selectedPrestation = selectedPrestation;
	}


	
	
	
	
}
