package fr.afcepf.ai107.appnomads.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

@Entity
@Table(name= "evolution_carriere")
public class EvolutionCarriereEntity implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue (strategy = GenerationType.IDENTITY)
	private Integer id;
	@Column(name = "date_debut_evolution")
	private Date dateDebutEvolution;
	@Column(name = "date_fin_evolution_statut")
	private Date dateFinEvolutionStatut;
	
	@ManyToOne
	@JoinColumn (referencedColumnName = "id")
	private StatutEntity statutEntity;
	@ManyToOne
	@JoinColumn (referencedColumnName = "id")
	private NiveauEntity niveauEntity;
	
	@ManyToOne
	@JoinColumn (referencedColumnName = "id")
	private MasseurEntity masseurEntity;
	
	public EvolutionCarriereEntity(Integer id, Date dateDebutEvolution, Date dateFinEvolutionStatut) {
		super();
		this.id = id;
		this.dateDebutEvolution = dateDebutEvolution;
		this.dateFinEvolutionStatut = dateFinEvolutionStatut;
;
	}



	public EvolutionCarriereEntity() {
		super();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getDateDebutEvolution() {
		return dateDebutEvolution;
	}

	public void setDateDebutEvolution(Date dateDebutEvolution) {
		this.dateDebutEvolution = dateDebutEvolution;
	}

	public Date getDateFinEvolutionStatut() {
		return dateFinEvolutionStatut;
	}

	public void setDateFinEvolutionStatut(Date dateFinEvolutionStatut) {
		this.dateFinEvolutionStatut = dateFinEvolutionStatut;
	}



	public static long getSerialversionuid() {
		return serialVersionUID;
	}



	public StatutEntity getStatutEntity() {
		return statutEntity;
	}



	public void setStatutEntity(StatutEntity statutEntity) {
		this.statutEntity = statutEntity;
	}



	public NiveauEntity getNiveauEntity() {
		return niveauEntity;
	}



	public void setNiveauEntity(NiveauEntity niveauEntity) {
		this.niveauEntity = niveauEntity;
	}



	public MasseurEntity getMasseurEntity() {
		return masseurEntity;
	}



	public void setMasseurEntity(MasseurEntity masseurEntity) {
		this.masseurEntity = masseurEntity;
	}



	public EvolutionCarriereEntity(Integer id, Date dateDebutEvolution, Date dateFinEvolutionStatut,
			StatutEntity statutEntity, NiveauEntity niveauEntity, MasseurEntity masseurEntity) {
		super();
		this.id = id;
		this.dateDebutEvolution = dateDebutEvolution;
		this.dateFinEvolutionStatut = dateFinEvolutionStatut;
		this.statutEntity = statutEntity;
		this.niveauEntity = niveauEntity;
		this.masseurEntity = masseurEntity;
	}



}
