package fr.afcepf.ai107.appnomads.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table (name = "libelle_duree_reccurence")
public class LibelleDureeReccurenceEntity implements Serializable
{
	
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue (strategy = GenerationType.IDENTITY)
	@Column (name = "id", nullable = false)
	int idLibelleDureeReccurence;
	@Column (name = "libelle")
	String libelle;
	@OneToMany (mappedBy = "libelleDureeReccurence", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	List<ReccurenceEntity> reccurences;

	/**
	 * @param idLibelleDureeReccurence
	 * @param libelle
	 * @param reccurences
	 */
	public LibelleDureeReccurenceEntity(int idLibelleDureeReccurence, String libelle,
			List<ReccurenceEntity> reccurences)
	{
		super();
		this.idLibelleDureeReccurence = idLibelleDureeReccurence;
		this.libelle = libelle;
		this.reccurences = reccurences;
	}
	

	/**
	 * 
	 */
	public LibelleDureeReccurenceEntity()
	{
		super();
		// TODO Auto-generated constructor stub
	}


	public int getIdLibelleDureeReccurence() {
		return idLibelleDureeReccurence;
	}

	public void setIdLibelleDureeReccurence(int idLibelleDureeReccurence) {
		this.idLibelleDureeReccurence = idLibelleDureeReccurence;
	}

	public String getLibelle() {
		return libelle;
	}

	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}

	public List<ReccurenceEntity> getReccurences() {
		return reccurences;
	}

	public void setReccurences(List<ReccurenceEntity> reccurences) {
		this.reccurences = reccurences;
	}
	
	
}
