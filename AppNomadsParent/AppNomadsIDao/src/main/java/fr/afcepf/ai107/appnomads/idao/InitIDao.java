package fr.afcepf.ai107.appnomads.idao;

import fr.afcepf.ai107.appnomads.entity.Init;

public interface InitIDao extends GenericIDao<Init>
{
	void vide();

}
