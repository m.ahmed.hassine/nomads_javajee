package fr.afcepf.ai107.appnomads.idao;

import fr.afcepf.ai107.appnomads.entity.MasseurEntity;
import fr.afcepf.ai107.appnomads.entity.StatutEntity;

public interface StatutIDao extends GenericIDao<StatutEntity> {

	StatutEntity getStatutByMasseur(MasseurEntity selectedMasseur);

}
