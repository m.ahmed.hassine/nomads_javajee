package fr.afcepf.ai107.appnomads.dao;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import fr.afcepf.ai107.appnomads.entity.LibelleMaterielEntity;
import fr.afcepf.ai107.appnomads.idao.LibelleMaterielIDao;

@Remote(LibelleMaterielIDao.class)
@Stateless
public class LibelleMaterielDao extends GenericDao<LibelleMaterielEntity> implements LibelleMaterielIDao{

	@PersistenceContext(unitName = "AppNomadsPU")
	private EntityManager em;
	
}
