package fr.afcepf.ai107.appnomads.dao;

import java.util.List;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import fr.afcepf.ai107.appnomads.entity.HistoriqueActiviteMasseurEntity;
import fr.afcepf.ai107.appnomads.entity.HistoriqueActivitePartenaireEntity;
import fr.afcepf.ai107.appnomads.entity.MasseurEntity;
import fr.afcepf.ai107.appnomads.entity.PartenaireEntity;
import fr.afcepf.ai107.appnomads.idao.HistoriqueActiviteIDao;

@Remote(HistoriqueActiviteIDao.class)
@Stateless
public class HistoriqueActiviteDao extends GenericDao<HistoriqueActiviteMasseurEntity> implements HistoriqueActiviteIDao {

	@PersistenceContext(unitName = "AppNomadsPU")
	private EntityManager em;
	
	@SuppressWarnings("unchecked")
	@Override
	public List<HistoriqueActiviteMasseurEntity> getListeHistoriqueActiviteByMasseur(MasseurEntity selectedMasseur) {
		List<HistoriqueActiviteMasseurEntity> liste = null;
		Query query = em.createQuery("SELECT h FROM HistoriqueActiviteMasseurEntity h WHERE h.masseurEntity = :paramMasseur"); 
		query.setParameter("paramMasseur", selectedMasseur);
		liste = query.getResultList();
		return liste;
	}

	@SuppressWarnings("unchecked")
	@Override
	public HistoriqueActiviteMasseurEntity getCurrentActiviteByMasseur(MasseurEntity selectedMasseur) {
		List<HistoriqueActiviteMasseurEntity> liste = null;
		Query query = em.createQuery("SELECT h FROM HistoriqueActiviteMasseurEntity h WHERE h.masseurEntity = :paramMasseur and h.dateFinActivite = null"); 
		query.setParameter("paramMasseur", selectedMasseur);
		liste = query.getResultList();
		return liste.get(0);
	}

}
