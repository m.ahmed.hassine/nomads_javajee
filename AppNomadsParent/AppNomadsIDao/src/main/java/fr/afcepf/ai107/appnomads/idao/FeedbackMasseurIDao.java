package fr.afcepf.ai107.appnomads.idao;

import java.util.List;

import fr.afcepf.ai107.appnomads.entity.FeedbackMasseurEntity;

public interface FeedbackMasseurIDao extends GenericIDao<FeedbackMasseurEntity>
{
	List<FeedbackMasseurEntity> getListeFeedbackMasseurByPrestation(int id);

}
