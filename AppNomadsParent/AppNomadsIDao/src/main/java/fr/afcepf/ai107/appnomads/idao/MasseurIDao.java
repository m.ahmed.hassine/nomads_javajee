package fr.afcepf.ai107.appnomads.idao;

import fr.afcepf.ai107.appnomads.entity.MasseurEntity;

public interface MasseurIDao extends GenericIDao<MasseurEntity> {

	boolean isActif(MasseurEntity selectedMasseur);
	

}
