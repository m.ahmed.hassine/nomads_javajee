package fr.afcepf.ai107.appnomads.IBusiness;

import fr.afcepf.ai107.appnomads.entity.FeedbackMasseurEntity;
import fr.afcepf.ai107.appnomads.entity.FeedbackPartenaireEntity;

public interface FeedbackIBusiness 
{
	FeedbackPartenaireEntity addFeedbackPartenaire(FeedbackPartenaireEntity feedbackPartenaire);
	FeedbackMasseurEntity addFeedbackMasseur(FeedbackMasseurEntity feedbackMasseur);

}
