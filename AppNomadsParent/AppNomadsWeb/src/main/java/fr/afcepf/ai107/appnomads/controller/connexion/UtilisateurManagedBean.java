package fr.afcepf.ai107.appnomads.controller.connexion;

import java.io.Serializable;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;

import fr.afcepf.ai107.appnomads.IBusiness.MasseurIBusiness;
import fr.afcepf.ai107.appnomads.entity.UtilisateurEntity;

@ManagedBean(name = "mbUser")
@SessionScoped
public class UtilisateurManagedBean implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@EJB
	private MasseurIBusiness proxyMasseurBu;
	
	@ManagedProperty (value = "#{mbAccount.utilisateur}" )
	private UtilisateurEntity utilisateur;
	
	public UtilisateurManagedBean() {
		// TODO Auto-generated constructor stub
	}

}
