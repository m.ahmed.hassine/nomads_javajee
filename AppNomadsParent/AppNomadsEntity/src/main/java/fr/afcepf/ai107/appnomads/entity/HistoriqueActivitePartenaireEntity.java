package fr.afcepf.ai107.appnomads.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "historique_activite_partenaire")
public class HistoriqueActivitePartenaireEntity implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue (strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Integer idHistoriqueActivitePartenaire;
	@Column(name = "date_debut_activite")
	private Date dateDebutActivite;
	@Column(name = "date_fin_activite")
	private Date dateFinActivite;
	
	//abdel

	@ManyToOne
	@JoinColumn (referencedColumnName = "id")
	private PartenaireEntity partenaireEntity;
	
	@ManyToOne
	@JoinColumn (referencedColumnName = "id")
	private MotifFinActivitePartenaireEntity motifFinActivitePartenaireEntity;
	
	//abdel
	
	public HistoriqueActivitePartenaireEntity() {
		super();
		// TODO Auto-generated constructor stub
	}

	public HistoriqueActivitePartenaireEntity(Integer idHistoriqueActivitePartenaire, Date dateDebutActivite,
			Date dateFinActivite) {
		super();
		this.idHistoriqueActivitePartenaire = idHistoriqueActivitePartenaire;
		this.dateDebutActivite = dateDebutActivite;
		this.dateFinActivite = dateFinActivite;
	}

	public Integer getIdHistoriqueActivitePartenaire() {
		return idHistoriqueActivitePartenaire;
	}

	public void setIdHistoriqueActivitePartenaire(Integer idHistoriqueActivitePartenaire) {
		this.idHistoriqueActivitePartenaire = idHistoriqueActivitePartenaire;
	}

	public Date getDateDebutActivite() {
		return dateDebutActivite;
	}

	public void setDateDebutActivite(Date dateDebutActivite) {
		this.dateDebutActivite = dateDebutActivite;
	}

	public Date getDateFinActivite() {
		return dateFinActivite;
	}

	public void setDateFinActivite(Date dateFinActivite) {
		this.dateFinActivite = dateFinActivite;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public void setPartenaire(PartenaireEntity partenaire) {
		this.partenaireEntity=partenaire;
		
	}
	

}
