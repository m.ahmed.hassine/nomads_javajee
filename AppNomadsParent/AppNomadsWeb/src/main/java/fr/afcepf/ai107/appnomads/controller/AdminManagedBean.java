package fr.afcepf.ai107.appnomads.controller;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;

import fr.afcepf.ai107.appnomads.IBusiness.connexion.UtilisateurIBusiness;
import fr.afcepf.ai107.appnomads.entity.AdministrateurEntity;
import fr.afcepf.ai107.appnomads.entity.MasseurEntity;
import fr.afcepf.ai107.appnomads.entity.PartenaireEntity;
import fr.afcepf.ai107.appnomads.entity.UtilisateurEntity;

@ManagedBean (name = "mbAdmin")
@RequestScoped
public class AdminManagedBean
{
	private AdministrateurEntity profilAdmin;
	private MasseurEntity profilMasseur;
	private PartenaireEntity profilPartenaire;
	
	
	
	@ManagedProperty (value = "#{mbAccount.utilisateur}" )
	private UtilisateurEntity utilisateur;
	
	@EJB
	UtilisateurIBusiness proxyUtilisateur;
	
	@PostConstruct
	public void diplayAccueil() {
		
		profilAdmin = proxyUtilisateur.getAdminById(utilisateur.getId());
		
	}
	public void showAdmin(int idAdmin) {
	}
	
	public AdministrateurEntity getProfilAdmin() {
		return profilAdmin;
	}

	public void setProfilAdmin(AdministrateurEntity profilAdmin) {
		this.profilAdmin = profilAdmin;
	}

	public MasseurEntity getProfilMasseur() {
		return profilMasseur;
	}

	public void setProfilMasseur(MasseurEntity profilMasseur) {
		this.profilMasseur = profilMasseur;
	}

	public PartenaireEntity getProfilPartenaire() {
		return profilPartenaire;
	}

	public void setProfilPartenaire(PartenaireEntity profilPartenaire) {
		this.profilPartenaire = profilPartenaire;
	}

	public UtilisateurEntity getUtilisateur() {
		return utilisateur;
	}

	public void setUtilisateur(UtilisateurEntity utilisateur) {
		this.utilisateur = utilisateur;
	}

	

	
	
	
	
	
	

}
