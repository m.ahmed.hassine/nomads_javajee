package fr.afcepf.ai107.appnomads.idao;

import fr.afcepf.ai107.appnomads.entity.MasseurEntity;
import fr.afcepf.ai107.appnomads.entity.TransactionEntity;

public interface TransactionIDao extends GenericIDao<TransactionEntity> {

	int getSoldeByMasseur(MasseurEntity masseur);
}
