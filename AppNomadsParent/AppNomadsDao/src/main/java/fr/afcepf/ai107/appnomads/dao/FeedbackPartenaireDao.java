package fr.afcepf.ai107.appnomads.dao;

import java.util.List;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import fr.afcepf.ai107.appnomads.entity.FeedbackPartenaireEntity;
import fr.afcepf.ai107.appnomads.entity.PartenaireEntity;
import fr.afcepf.ai107.appnomads.entity.PrestationEntity;
import fr.afcepf.ai107.appnomads.idao.FeedbackPartenaireIDao;

@Remote (FeedbackPartenaireIDao.class)
@Stateless
public class FeedbackPartenaireDao extends GenericDao<FeedbackPartenaireEntity> implements FeedbackPartenaireIDao
{
	@PersistenceContext(unitName = "AppNomadsPU")
	private EntityManager em;

	@Override
	public List<FeedbackPartenaireEntity> getListeFeedbackPartenaireByPrestation(int id) {
		List<FeedbackPartenaireEntity> feedbackPartenaires = null;
		Query query = em.createQuery("SELECT f FROM FeedbackPartenaireEntity f WHERE f.partenaireEntity.id = :paramIdPrestation"); 
		query.setParameter("paramIdPrestation", id);
		feedbackPartenaires = query.getResultList();
		return feedbackPartenaires;
	}

	
	}


