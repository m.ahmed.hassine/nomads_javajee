package fr.afcepf.ai107.appnomads.idao;

import java.util.List;

import fr.afcepf.ai107.appnomads.entity.InscriptionEntity;

public interface InscriptionIDao extends GenericIDao<InscriptionEntity>
{
	List<InscriptionEntity> getListInscriptionByPrestationID(int id);
}
