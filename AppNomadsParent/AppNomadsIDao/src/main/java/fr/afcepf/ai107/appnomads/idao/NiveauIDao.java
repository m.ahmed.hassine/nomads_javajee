package fr.afcepf.ai107.appnomads.idao;

import fr.afcepf.ai107.appnomads.entity.MasseurEntity;
import fr.afcepf.ai107.appnomads.entity.NiveauEntity;

public interface NiveauIDao extends GenericIDao<NiveauEntity> {

	NiveauEntity getNiveauByMasseur(MasseurEntity selectedMasseur);

}
