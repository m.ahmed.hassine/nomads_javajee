package fr.afcepf.ai107.appnomads.dao;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import fr.afcepf.ai107.appnomads.entity.Init;
import fr.afcepf.ai107.appnomads.idao.InitIDao;

@Remote (InitIDao.class)
@Stateless
public class InitDao extends GenericDao<Init> implements InitIDao
{
	@PersistenceContext(unitName = "AppNomadsPU")
	private EntityManager em;


	@Override
	public void vide() {
		// TODO Auto-generated method stub
		
	}

}
