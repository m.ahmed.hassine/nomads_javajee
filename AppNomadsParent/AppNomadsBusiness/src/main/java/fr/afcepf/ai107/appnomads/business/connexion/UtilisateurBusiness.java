package fr.afcepf.ai107.appnomads.business.connexion;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;

import fr.afcepf.ai107.appnomads.IBusiness.connexion.UtilisateurIBusiness;
import fr.afcepf.ai107.appnomads.entity.AdministrateurEntity;
import fr.afcepf.ai107.appnomads.entity.MasseurEntity;
import fr.afcepf.ai107.appnomads.entity.PartenaireEntity;
import fr.afcepf.ai107.appnomads.entity.UtilisateurEntity;
import fr.afcepf.ai107.appnomads.idao.AdministrateurIDao;
import fr.afcepf.ai107.appnomads.idao.MasseurIDao;
import fr.afcepf.ai107.appnomads.idao.PartenaireIDao;
import fr.afcepf.ai107.appnomads.idao.UtilisateurIDao;



@Remote(UtilisateurIBusiness.class)
@Stateless
public class UtilisateurBusiness implements UtilisateurIBusiness {

	@EJB
	private UtilisateurIDao proxyUtilisateurDao;
	@EJB
	private MasseurIDao proxyMasseurDao;
	@EJB
	private AdministrateurIDao proxyAdminDao;
	@EJB
	private PartenaireIDao proxyPartenaireDao;
	

	
	
	@Override
	public List<UtilisateurEntity> displayAllUtilisateur() {
		// TODO Auto-generated method stub
		return proxyUtilisateurDao.findAll();
	}

	@Override
	public UtilisateurEntity connexion(String login, String password) {
		// TODO Auto-generated method stub
		return proxyUtilisateurDao.authenticate(login, password);
	}

	@Override
	public AdministrateurEntity getAdminById(int id) {
		// TODO Auto-generated method stub
		return proxyAdminDao.getById(id);
	}

	@Override
	public PartenaireEntity getPartenaireById(int id) {
		// TODO Auto-generated method stub
		return proxyPartenaireDao.getById(id);
	}

	@Override
	public MasseurEntity getMasseurById(int id) {
		// TODO Auto-generated method stub
		return proxyMasseurDao.getById(id);
	}

}