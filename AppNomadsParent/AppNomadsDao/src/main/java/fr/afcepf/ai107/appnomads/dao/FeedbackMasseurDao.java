package fr.afcepf.ai107.appnomads.dao;

import java.util.List;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import fr.afcepf.ai107.appnomads.entity.FeedbackMasseurEntity;
import fr.afcepf.ai107.appnomads.idao.FeedbackMasseurIDao;

@Remote(FeedbackMasseurIDao.class)
@Stateless
public class FeedbackMasseurDao extends GenericDao<FeedbackMasseurEntity> implements FeedbackMasseurIDao
{
	@PersistenceContext(unitName = "AppNomadsPU")
	private EntityManager em;
	
	@Override
	public List<FeedbackMasseurEntity> getListeFeedbackMasseurByPrestation(int id) {
		List<FeedbackMasseurEntity> feedbackMasseurs = null;
		Query query = em.createQuery("SELECT f FROM FeedbackMasseurEntity f WHERE f.prestationEntity.id = :paramIdPrestation"); 
		query.setParameter("paramIdPrestation", id);
		feedbackMasseurs = query.getResultList();
		return feedbackMasseurs;
	}

}
