package fr.afcepf.ai107.appnomads.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "libelle_materiel")
public class LibelleMaterielEntity implements Serializable {
	
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue (strategy = GenerationType.IDENTITY)
	private Integer id;
	@Column(name = "libelle")
	private String libelle;
	@Column(name = "caution_mads")
	private int cautionMads;
	
	@OneToMany (mappedBy = "libelleEntity", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private List<EmpruntMaterielEntity> empruntMateriel;
	


	public LibelleMaterielEntity(Integer id, String libelle, int cautionMads) {
		super();
		this.id = id;
		this.libelle = libelle;
		this.cautionMads = cautionMads;
	}

	public LibelleMaterielEntity() {
		super();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getLibelle() {
		return libelle;
	}

	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}

	public int getCautionMads() {
		return cautionMads;
	}

	public void setCautionMads(int cautionMads) {
		this.cautionMads = cautionMads;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}



}
