package fr.afcepf.ai107.appnomads.IBusiness;

import java.util.List;

import fr.afcepf.ai107.appnomads.entity.EmpruntMaterielEntity;
import fr.afcepf.ai107.appnomads.entity.LibelleMaterielEntity;
import fr.afcepf.ai107.appnomads.entity.MasseurEntity;

public interface LocationMaterielIBusiness {
	
	List<LibelleMaterielEntity> displayAllMateriel();

	
	EmpruntMaterielEntity addEmpruntMateriel(EmpruntMaterielEntity empruntMateriel);
	
	
	
}
