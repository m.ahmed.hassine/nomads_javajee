package fr.afcepf.ai107.appnomads.idao;

import java.util.List;

import fr.afcepf.ai107.appnomads.entity.EmpruntMaterielEntity;

public interface EmpruntMaterielIDao extends GenericIDao<EmpruntMaterielEntity>{


	List<EmpruntMaterielEntity> getListeEmpruntMaterielByMasseur1(int id);

	
}
