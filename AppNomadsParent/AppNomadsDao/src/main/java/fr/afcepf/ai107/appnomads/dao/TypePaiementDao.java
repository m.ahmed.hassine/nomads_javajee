package fr.afcepf.ai107.appnomads.dao;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import fr.afcepf.ai107.appnomads.entity.TypePaiementEntity;
import fr.afcepf.ai107.appnomads.idao.TypePaiementIDao;

@Remote (TypePaiementIDao.class)
@Stateless
public class TypePaiementDao extends GenericDao<TypePaiementEntity> implements TypePaiementIDao
{
	@PersistenceContext(unitName = "AppNomadsPU")
	private EntityManager em;
}
