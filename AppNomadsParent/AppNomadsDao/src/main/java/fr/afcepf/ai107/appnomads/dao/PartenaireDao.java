package fr.afcepf.ai107.appnomads.dao;


import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import fr.afcepf.ai107.appnomads.entity.PartenaireEntity;
import fr.afcepf.ai107.appnomads.idao.PartenaireIDao;

@Remote (PartenaireIDao.class)
@Stateless
public class PartenaireDao extends GenericDao<PartenaireEntity> implements PartenaireIDao
{

	@PersistenceContext(unitName = "AppNomadsPU")
	private EntityManager em;
	

}
