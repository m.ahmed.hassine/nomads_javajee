package fr.afcepf.ai107.appnomads.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table (name = "motif_annulation")
public class MotifAnnulationEntity implements Serializable
{
	
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue (strategy = GenerationType.IDENTITY)
	@Column (name = "id", nullable = false)
	private Integer id;
	@Column (name = "libelle_motif_annulation")
	String libelleMotifAnnulation;
	@OneToMany (mappedBy = "motifAnnulation", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	List<PrestationEntity> prestationEntiy;

	/**
	 * @param idMotifAnnulation
	 * @param libelleMotifAnnulation
	 * @param prestations
	 */
	public MotifAnnulationEntity(int idMotifAnnulation, String libelleMotifAnnulation,
			List<PrestationEntity> prestations)
	{
		super();
		this.id = idMotifAnnulation;
		this.libelleMotifAnnulation = libelleMotifAnnulation;
		this.prestationEntiy = prestations;
	}
	

	/**
	 * 
	 */
	public MotifAnnulationEntity()
	{
		super();
		// TODO Auto-generated constructor stub
	}


	public int getIdMotifAnnulation() {
		return id;
	}

	public void setIdMotifAnnulation(int idMotifAnnulation) {
		this.id = idMotifAnnulation;
	}

	public String getLibelleMotifAnnulation() {
		return libelleMotifAnnulation;
	}

	public void setLibelleMotifAnnulation(String libelleMotifAnnulation) {
		this.libelleMotifAnnulation = libelleMotifAnnulation;
	}

	public List<PrestationEntity> getPrestations() {
		return prestationEntiy;
	}

	public void setPrestations(List<PrestationEntity> prestations) {
		this.prestationEntiy = prestations;
	}
	
	
}
