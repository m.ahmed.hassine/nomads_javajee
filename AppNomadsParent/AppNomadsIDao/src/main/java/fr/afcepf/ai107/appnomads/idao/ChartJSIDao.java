package fr.afcepf.ai107.appnomads.idao;

import java.util.List;

import fr.afcepf.ai107.appnomads.entity.MasseurEntity;
import fr.afcepf.ai107.appnomads.entity.PartenaireEntity;
import fr.afcepf.ai107.appnomads.entity.PrestationEntity;

public interface ChartJSIDao extends GenericIDao<PrestationEntity>{
	
	List<PrestationEntity> getListePrestationByMasseur(MasseurEntity masseur);

	List<PrestationEntity> getListePrestationByPartenaire(PartenaireEntity partenaire);
	
}
