package fr.afcepf.ai107.appnomads.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.inject.Named;

import org.primefaces.event.ItemSelectEvent;
import org.primefaces.model.charts.ChartData;
import org.primefaces.model.charts.axes.cartesian.CartesianScales;
import org.primefaces.model.charts.axes.cartesian.linear.CartesianLinearAxes;
import org.primefaces.model.charts.axes.cartesian.linear.CartesianLinearTicks;
import org.primefaces.model.charts.bar.BarChartDataSet;
import org.primefaces.model.charts.bar.BarChartModel;
import org.primefaces.model.charts.bar.BarChartOptions;
import org.primefaces.model.charts.optionconfig.title.Title;

import fr.afcepf.ai107.appnomads.IBusiness.ChartJSIBusiness;
import fr.afcepf.ai107.appnomads.IBusiness.MasseurIBusiness;

@ManagedBean(name = "mbChartJS")
@RequestScoped
public class ChartJSManagedBean implements Serializable
{

	private static final long serialVersionUID = 1L;

	private BarChartModel barModel2;
	private Integer nbMasseur;
	private Integer nbPartenaire;
	private Integer nbPrestation;
	private Integer totalCA;

	@EJB
	private ChartJSIBusiness proxyChartJSbu;
	
	@EJB
	private MasseurIBusiness proxyMasseurBu;

	public Integer getNbPrestation() {
		return nbPrestation;
	}

	public void setNbPrestation(Integer nbPrestation) {
		this.nbPrestation = nbPrestation;
	}

	public Integer getTotalCA() {
		return totalCA;
	}

	public void setTotalCA(Integer totalCA) {
		this.totalCA = totalCA;
	}

	public ChartJSIBusiness getProxyChartJSbu() {
		return proxyChartJSbu;
	}

	public void setProxyChartJSbu(ChartJSIBusiness proxyChartJSbu) {
		this.proxyChartJSbu = proxyChartJSbu;
	}

	@PostConstruct
	public void init() {
		createBarModel2();
		nbMasseur = proxyMasseurBu.nbMasseurTotal();
		nbPartenaire = proxyChartJSbu.nbPartenaire();
		nbPrestation = proxyChartJSbu.nbPrestation();
		totalCA = proxyChartJSbu.totalCA();

	}

	public void createBarModel2() {
		barModel2 = new BarChartModel();
		ChartData data = new ChartData();

		BarChartDataSet barDataSet = new BarChartDataSet();
		barDataSet.setLabel("Nbre prestations prévues 2020");
		barDataSet.setBackgroundColor("rgba(57, 15, 224, 0.2)");
		barDataSet.setBorderColor("rgb(255, 99, 132)");
		barDataSet.setBorderWidth(1);
		List<Number> values = new ArrayList<>();
		values.add(35);
		values.add(40);
		values.add(42);
		values.add(12);
		values.add(05);
		values.add(00);
		values.add(10);
		barDataSet.setData(values);

		BarChartDataSet barDataSet2 = new BarChartDataSet();
		barDataSet2.setLabel("Nbre prestations réalisées 2020");
		barDataSet2.setBackgroundColor("rgba(183, 167, 64, 0.2)");
		barDataSet2.setBorderColor("rgb(76, 255, 0)");
		barDataSet2.setBorderWidth(2);
		List<Number> values2 = new ArrayList<>();
		values2.add(34);
		values2.add(32);
		values2.add(05);
		values2.add(01);
		values2.add(00);
		values2.add(00);
		values2.add(00);
		barDataSet2.setData(values2);
		
//		BarChartDataSet barDataSet3 = new BarChartDataSet();
//		barDataSet3.setLabel("My Third Dataset");
//		barDataSet3.setBackgroundColor("rgba(255, 159, 12, 0.2)");
//		barDataSet3.setBorderColor("rgb(250, 9, 12)");
//		barDataSet3.setBorderWidth(3);
//		List<Number> values3 = new ArrayList<>();
//		values3.add(15);
//		values3.add(59);
//		values3.add(90);
//		values3.add(41);
//		values3.add(36);
//		values3.add(25);
//		values3.add(78);
//		barDataSet3.setData(values3);

		data.addChartDataSet(barDataSet);
		data.addChartDataSet(barDataSet2);
//		data.addChartDataSet(barDataSet3);

		List<String> labels = new ArrayList<>();
		labels.add("Janvier");
		labels.add("Février");
		labels.add("Mars");
		labels.add("Avril");
		labels.add("Mai");
		labels.add("Juin");
//		labels.add("August");
//		labels.add("September");
//		labels.add("October");
//		labels.add("November");
//		labels.add("December");
		data.setLabels(labels);
		barModel2.setData(data);

		// Options
		BarChartOptions options = new BarChartOptions();
		CartesianScales cScales = new CartesianScales();
		CartesianLinearAxes linearAxes = new CartesianLinearAxes();
		CartesianLinearTicks ticks = new CartesianLinearTicks();
		ticks.setBeginAtZero(true);
		linearAxes.setTicks(ticks);
		cScales.addYAxesData(linearAxes);
		options.setScales(cScales);

		Title title = new Title();
		title.setDisplay(true);
		title.setText("Bar Chart");
		options.setTitle(title);

		barModel2.setOptions(options);
	}

	public void itemSelect(ItemSelectEvent event) {
		FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Item selected",
				"Item Index: " + event.getItemIndex() + ", DataSet Index:" + event.getDataSetIndex());

		FacesContext.getCurrentInstance().addMessage(null, msg);
	}

	public BarChartModel getBarModel2() {
		return barModel2;
	}

	public void setBarModel2(BarChartModel barModel2) {
		this.barModel2 = barModel2;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Integer getNbPartenaire() {
		return nbPartenaire;
	}

	public void setNbPartenaire(Integer nbPartenaire) {
		this.nbPartenaire = nbPartenaire;
	}

	public Integer getNbMasseur() {
		return nbMasseur;
	}

	public void setNbMasseur(Integer nbMasseur) {
		this.nbMasseur = nbMasseur;
	}

}
