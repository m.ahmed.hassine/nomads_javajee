package fr.afcepf.ai107.appnomads.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name= "historique_activite_masseur")
public class HistoriqueActiviteMasseurEntity implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue (strategy = GenerationType.IDENTITY)
	private Integer id;
	@Column(name = "date_debut_activite")
	private Date dateDebutActivite;
	@Column(name = "date_fin_activite")
	private Date dateFinActivite;
	
	@ManyToOne
	@JoinColumn (referencedColumnName = "id")
	private MotifFinActiviteEntity motif;
	@ManyToOne
	@JoinColumn (referencedColumnName = "id")
	private MasseurEntity masseurEntity;
	
	public HistoriqueActiviteMasseurEntity(Integer id, Date dateDebutActivite, Date dateFinActivite,
			MotifFinActiviteEntity motif) {
		super();
		this.id = id;
		this.dateDebutActivite = dateDebutActivite;
		this.dateFinActivite = dateFinActivite;
		this.motif = motif;
	}

	public HistoriqueActiviteMasseurEntity(Integer id, Date dateDebutActivite, Date dateFinActivite,
			MotifFinActiviteEntity motif, MasseurEntity masseur) {
		super();
		this.id = id;
		this.dateDebutActivite = dateDebutActivite;
		this.dateFinActivite = dateFinActivite;
		this.motif = motif;
		this.masseurEntity = masseur;
	}

	public HistoriqueActiviteMasseurEntity() {
		super();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getDateDebutActivite() {
		return dateDebutActivite;
	}

	public void setDateDebutActivite(Date dateDebutActivite) {
		this.dateDebutActivite = dateDebutActivite;
	}

	public Date getDateFinActivite() {
		return dateFinActivite;
	}

	public void setDateFinActivite(Date dateFinActivite) {
		this.dateFinActivite = dateFinActivite;
	}

	public MotifFinActiviteEntity getMotif() {
		return motif;
	}

	public void setMotif(MotifFinActiviteEntity motif) {
		this.motif = motif;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public MasseurEntity getMasseur() {
		return masseurEntity;
	}

	public void setMasseur(MasseurEntity masseur) {
		this.masseurEntity = masseur;
	}

	public MasseurEntity getMasseurEntity() {
		return masseurEntity;
	}

	public void setMasseurEntity(MasseurEntity masseurEntity) {
		this.masseurEntity = masseurEntity;
	}

}
