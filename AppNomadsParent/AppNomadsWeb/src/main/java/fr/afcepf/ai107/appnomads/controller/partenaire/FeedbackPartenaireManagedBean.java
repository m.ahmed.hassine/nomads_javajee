package fr.afcepf.ai107.appnomads.controller.partenaire;

import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;

import fr.afcepf.ai107.appnomads.IBusiness.FeedbackIBusiness;
import fr.afcepf.ai107.appnomads.IBusiness.PartenaireIBusiness;
import fr.afcepf.ai107.appnomads.entity.FeedbackPartenaireEntity;
import fr.afcepf.ai107.appnomads.entity.PartenaireEntity;
import fr.afcepf.ai107.appnomads.entity.PrestationEntity;
import fr.afcepf.ai107.appnomads.entity.UtilisateurEntity;
import fr.afcepf.ai107.appnomds.ibusiness.prestation.PrestationIBusiness;

@ManagedBean (name = "mbFeedbackPartenaire")
@RequestScoped
public class FeedbackPartenaireManagedBean {

	private PartenaireEntity partenaire = new PartenaireEntity();
	private FeedbackPartenaireEntity feedbackPartenaire = new FeedbackPartenaireEntity();
	private List<PrestationEntity> listePrestations;

	@ManagedProperty (value = "#{mbPartenaires.selectedPrestation}")
	private PrestationEntity selectedPrestation;
	
	@ManagedProperty (value = "#{mbAccount.profilPartenaire}")
	private UtilisateurEntity user;
	 
	
	@EJB
	private FeedbackIBusiness proxyFeedbackPartenaireBu;
	@EJB
	private PartenaireIBusiness proxyPartenairesBu;
	@EJB
	private PrestationIBusiness proxyPrestationBu;
	
	public void AjouterFeedbackPartenaire() {
		//Persister le feedback
		partenaire = proxyPartenairesBu.getPartenaireById(user.getId());
		feedbackPartenaire.setPartenaireEntity(partenaire);
		feedbackPartenaire.setPrestationEntity(selectedPrestation);
		feedbackPartenaire.setDateFeedback(new Date());
		feedbackPartenaire = proxyFeedbackPartenaireBu.addFeedbackPartenaire( feedbackPartenaire);

}		
	public String feedbackPage(int id) {
		String retour = null;
		selectedPrestation = proxyPrestationBu.findPrestation(id);
		if(selectedPrestation != null) {
			retour = "/partenaire/FeedbackPartenaire.xhtml?faces-redirect=true";
		}
		
		return retour;
	}
	@PostConstruct
	public void init() {
		
//		listePrestations = proxyFeedbackPartenaireBu.displayAllPrestation();
	}
	


	
	

	public PartenaireEntity getPartenaire() {
		return partenaire;
	}

	public void setPartenaire(PartenaireEntity partenaire) {
		this.partenaire = partenaire;
	}

	public FeedbackPartenaireEntity getFeedbackPartenaire() {
		return feedbackPartenaire;
	}

	public void setFeedbackPartenaire(FeedbackPartenaireEntity feedbackPartenaire) {
		this.feedbackPartenaire = feedbackPartenaire;
	}

	public List<PrestationEntity> getListePrestations() {
		return listePrestations;
	}

	public void setListePrestations(List<PrestationEntity> listePrestations) {
		this.listePrestations = listePrestations;
	}

	public PrestationEntity getSelectedPrestation() {
		return selectedPrestation;
	}

	public void setSelectedPrestation(PrestationEntity selectedPrestation) {
		this.selectedPrestation = selectedPrestation;
	}

	public UtilisateurEntity getUser() {
		return user;
	}

	public void setUser(UtilisateurEntity user) {
		this.user = user;
	}

	



	
}