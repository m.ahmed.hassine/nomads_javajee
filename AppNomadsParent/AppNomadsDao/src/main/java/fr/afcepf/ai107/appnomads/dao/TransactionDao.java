package fr.afcepf.ai107.appnomads.dao;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import fr.afcepf.ai107.appnomads.entity.MasseurEntity;
import fr.afcepf.ai107.appnomads.entity.TransactionEntity;
import fr.afcepf.ai107.appnomads.entity.TypePacksEntity;
import fr.afcepf.ai107.appnomads.idao.TransactionIDao;
import fr.afcepf.ai107.appnomads.idao.TypePacksIDao;

@Remote (TransactionIDao.class)
@Stateless
public class TransactionDao extends GenericDao<TransactionEntity> implements TransactionIDao {

	@PersistenceContext(unitName = "AppNomadsPU")
	private EntityManager em;
	
	@EJB
	private TypePacksIDao proxyTypePacksDao;
	

	@Override
	public int getSoldeByMasseur(MasseurEntity masseur) {
		int solde = 0;
		int achats = 0;
		int depenses =0;
		List<TransactionEntity> transactions;
		TypePacksEntity typePacks;
		//Récupérer toutes les transactions du masseur
		Query query = em.createQuery("SELECT t FROM TransactionEntity t WHERE t.masseur = :paramMasseur"); 
		query.setParameter("paramMasseur", masseur);
		transactions = query.getResultList();
		//Calcul du solde (les débits sont en valeurs négatifs et les crédits en valeurs positifs)
		for (TransactionEntity transaction : transactions) {
			//Si quantite_packs <0 dépenses en mads
			if (transaction.getQuantitePack()<0) {
				depenses = transaction.getQuantitePack();
			} else {
				//Recuperer le type Packs
				typePacks = proxyTypePacksDao.getById(transaction.getTypePacksEntity().getId());
				//Calculer les achats
				achats = transaction.getQuantitePack()* typePacks.getQuantiteMads();
			}
		}
		
	return solde = achats + depenses;
	}

}
