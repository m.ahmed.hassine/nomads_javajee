package fr.afcepf.ai107.appnomads.IBusiness;

import java.util.List;

import fr.afcepf.ai107.appnomads.entity.AdresseEntity;
import fr.afcepf.ai107.appnomads.entity.EvolutionCarriereEntity;
import fr.afcepf.ai107.appnomads.entity.GenreEntity;
import fr.afcepf.ai107.appnomads.entity.HistoriqueActiviteMasseurEntity;
import fr.afcepf.ai107.appnomads.entity.MasseurEntity;
import fr.afcepf.ai107.appnomads.entity.NiveauEntity;
import fr.afcepf.ai107.appnomads.entity.PreferencesEntity;
import fr.afcepf.ai107.appnomads.entity.PrestationEntity;
import fr.afcepf.ai107.appnomads.entity.StatutEntity;
import fr.afcepf.ai107.appnomads.entity.TransactionEntity;
import fr.afcepf.ai107.appnomads.entity.TypePacksEntity;

public interface MasseurIBusiness {

	List<GenreEntity> displayAllGenre();

	MasseurEntity addMasseur(MasseurEntity masseur);

	AdresseEntity addAdresse(AdresseEntity adresse);

	EvolutionCarriereEntity addEvolutionCarriere(EvolutionCarriereEntity evolutionCarriere);

	HistoriqueActiviteMasseurEntity addHistoriqueActivite(HistoriqueActiviteMasseurEntity historiqueActivite);

	MasseurEntity getMasseurById(int i);

	List<PrestationEntity> getListePrestationsByMasseur(MasseurEntity masseur);

	List<PreferencesEntity> getListePreferencesByMasseur(MasseurEntity masseur);

	List<MasseurEntity> findAll();

	StatutEntity getStatutByMasseur(MasseurEntity selectedMasseur);

	NiveauEntity getNiveauByMasseur(MasseurEntity selectedMasseur);

	boolean isActif(MasseurEntity selectedMasseur);

	void updateMasseur(MasseurEntity selectedMasseur);

	List<HistoriqueActiviteMasseurEntity> getListeHistoriqueActiviteByMasseur(MasseurEntity selectedMasseur);
	
	Integer nbMasseurTotal();

	List<TypePacksEntity> getListeTypePacks();

	void addTransaction(TransactionEntity transaction);

	float getSoldeByMasseur(MasseurEntity utilisateur);

	void desactiverMasseur(MasseurEntity selectedMasseur);


}
