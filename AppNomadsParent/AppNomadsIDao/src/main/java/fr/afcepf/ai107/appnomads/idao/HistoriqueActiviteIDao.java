package fr.afcepf.ai107.appnomads.idao;

import java.util.List;

import fr.afcepf.ai107.appnomads.entity.HistoriqueActiviteMasseurEntity;
import fr.afcepf.ai107.appnomads.entity.MasseurEntity;

public interface HistoriqueActiviteIDao extends GenericIDao<HistoriqueActiviteMasseurEntity> {

	List<HistoriqueActiviteMasseurEntity> getListeHistoriqueActiviteByMasseur(MasseurEntity selectedMasseur);
	HistoriqueActiviteMasseurEntity getCurrentActiviteByMasseur(MasseurEntity selectedMasseur);
}
