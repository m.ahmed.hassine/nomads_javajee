package fr.afcepf.ai107.appnomads.idao;

import java.util.List;



import fr.afcepf.ai107.appnomads.entity.FeedbackPartenaireEntity;

public interface FeedbackPartenaireIDao extends GenericIDao<FeedbackPartenaireEntity>
{
	
	public List<FeedbackPartenaireEntity> getListeFeedbackPartenaireByPrestation(int id);
	

}
