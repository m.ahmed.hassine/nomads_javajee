package fr.afcepf.ai107.appnomads.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "adresse")
public class AdresseEntity implements Serializable{
	
	private static final long serialVersionUID = 1L;
	


	@Id
	@GeneratedValue (strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Integer idVille;
	@Column(name = "ville")
	private String ville;
	@Column(name = "code_postal")
	private String codePostal;
	@Column(name = "adresse")
	private String adresse;
	@Column(name ="complement_adresse")
	private String complementAdresse;
	
	@OneToMany (mappedBy = "adresseEntity", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private List<MasseurEntity> masseurEntity;
	
	@OneToMany (mappedBy = "adresseEntity", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private List<PrestationEntity> prestationEntity;
	
	@OneToMany (mappedBy = "adresseEntity", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private List<PartenaireEntity> partenaireEntity;
	
	public AdresseEntity() {
		super();
	}

	public AdresseEntity(Integer idVille, String ville, String codePostal, String adresse, String complementAdresse) {
		super();
		this.idVille = idVille;
		this.ville = ville;
		this.codePostal = codePostal;
		this.adresse = adresse;
		this.complementAdresse = complementAdresse;
	}
	public AdresseEntity(Integer idVille, String ville, String codePostal, String adresse, String complementAdresse,
			List<MasseurEntity> masseurEntity, List<PrestationEntity> prestationEntity,
			List<PartenaireEntity> partenaireEntity) {
		super();
		this.idVille = idVille;
		this.ville = ville;
		this.codePostal = codePostal;
		this.adresse = adresse;
		this.complementAdresse = complementAdresse;
		this.masseurEntity = masseurEntity;
		this.prestationEntity = prestationEntity;
		this.partenaireEntity = partenaireEntity;
	}
	public Integer getIdVille() {
		return idVille;
	}

	public void setIdVille(Integer idVille) {
		this.idVille = idVille;
	}

	public String getVille() {
		return ville;
	}

	public void setVille(String ville) {
		this.ville = ville;
	}

	public String getCodePostal() {
		return codePostal;
	}

	public void setCodePostal(String codePostal) {
		this.codePostal = codePostal;
	}

	public String getAdresse() {
		return adresse;
	}

	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}

	public String getComplementAdresse() {
		return complementAdresse;
	}

	public void setComplementAdresse(String complementAdresse) {
		this.complementAdresse = complementAdresse;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public List<MasseurEntity> getMasseurEntity() {
		return masseurEntity;
	}

	public void setMasseurEntity(List<MasseurEntity> masseurEntity) {
		this.masseurEntity = masseurEntity;
	}

	public List<PrestationEntity> getPrestationEntity() {
		return prestationEntity;
	}

	public void setPrestationEntity(List<PrestationEntity> prestationEntity) {
		this.prestationEntity = prestationEntity;
	}

	public List<PartenaireEntity> getPartenaireEntity() {
		return partenaireEntity;
	}

	public void setPartenaireEntity(List<PartenaireEntity> partenaireEntity) {
		this.partenaireEntity = partenaireEntity;
	}
	
}
