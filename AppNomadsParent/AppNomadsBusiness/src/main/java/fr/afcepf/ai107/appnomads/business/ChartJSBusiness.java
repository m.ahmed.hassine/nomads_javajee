package fr.afcepf.ai107.appnomads.business;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;

import fr.afcepf.ai107.appnomads.IBusiness.ChartJSIBusiness;
import fr.afcepf.ai107.appnomads.entity.PartenaireEntity;
import fr.afcepf.ai107.appnomads.entity.PrestationEntity;
import fr.afcepf.ai107.appnomads.idao.MasseurIDao;
import fr.afcepf.ai107.appnomads.idao.PartenaireIDao;
import fr.afcepf.ai107.appnomads.idao.PrestationIDao;

@Remote (ChartJSIBusiness.class)
@Stateless
public class ChartJSBusiness implements ChartJSIBusiness{

	
	@EJB
	private PrestationIDao proxyPrestationDao;
	
	@EJB
	private MasseurIDao proxyMasseurDao;
	
	@EJB
	private PartenaireIDao proxyPartenaireDao;
	


	@Override
	public List<PrestationEntity> getListePrestationsByPartenaire(PartenaireEntity partenaire) {
		return proxyPrestationDao.getListePrestationByPartenaire(partenaire);

	}


	@Override
	public Integer nbMasseur() {
		return proxyMasseurDao.findAll().size();
	}


	@Override
	public Integer nbPartenaire() {
		return proxyPartenaireDao.findAll().size();
	}


	@Override
	public Integer nbPrestation() {
		return proxyPrestationDao.findAll().size();
	}


	@Override
	public Integer totalCA() {
		// TODO Auto-generated method stub
		return null;
	}

	
}
